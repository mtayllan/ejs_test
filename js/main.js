$(document).ready(function(){

    // Render ejs 
    var template = $('#template').html();
    var data = {
        title: 'MBA em experiência...',
        lesson: {
            number: 5, 
            name: 'Professor John Doe',
            info: 'Informação do Módulo',
            date_time: '22/09/2019 15:09',
            files: [
                { name: 'Teoria1.pdf', link: 'file1.pdf'},
                { name: 'Slides.ppt', link: 'file2.pdf'},
                { name: 'Atividades.docx', link: 'file3.pdf'},                   
            ],
            messages: [
                { userType: 1, text: 'Professor ruim'},
                { userType: 2, text: 'Olá João, poderia nos detalhar melhor sobre sua insatisfação?'},
            ]            
        }
    };
    $('body').html(ejs.render(template, data)); 


    // Starts Sidenav
    var sidenav = M.Sidenav.init($('.sidenav'));

    // Starts Tabs
    var tabs = M.Tabs.init($('.tabs'), {swipeable: false});

    // Start Collapsible
    var collapsible = M.Collapsible.init($('.collapsible'), 
    { 
        onOpenStart: function(){
            $("#expand-icon").html('expand_less');
        },
        onCloseStart: function(){
            $("#expand-icon").html('expand_more');
        }
    });

    // Configura cor do icone de enviar
    $('#send-text').focusin(function (){
        $('#send-button').css('color', '#4BB0B8')
    });

    $('#send-text').focusout(function (){
        $('#send-button').css('color', '#666')
    });

});